import { Component, OnInit } from '@angular/core';
import { MobilePhonesService } from '../mobile-phones/mobilephones.service';
import { Subcategory } from '../shared/shared.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  showMobileMenu: boolean = false;
  
  public subcategories: Subcategory[] = [];

  constructor(
    private _phonesService: MobilePhonesService
  ) { }

  ngOnInit(): void {
    this.subcategories = this._phonesService.getSubcategories();
  }

}
