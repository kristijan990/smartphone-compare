import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Phones } from '../shared.model';

@Component({
  selector: 'app-phone-card',
  templateUrl: './phone-card.component.html',
  styleUrls: ['./phone-card.component.scss']
})
export class PhoneCardComponent implements OnInit {

  @Input() phoneDetails;

  public phones: Phones[];

  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
  }

  getDetails() {
    this._router.navigate(['phone' , this.phoneDetails.phoneId]);
  }
}
