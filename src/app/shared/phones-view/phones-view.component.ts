import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { MobilePhonesService } from 'src/app/mobile-phones/mobilephones.service';
import { Phones, Subcategory } from '../shared.model';

@Component({
  selector: 'app-phones-view',
  templateUrl: './phones-view.component.html',
  styleUrls: ['./phones-view.component.scss']
})
export class PhonesViewComponent implements OnInit, OnDestroy {

  public phones: Phones[];
  public subcategories: Subcategory[];

  private _unsubsrcibe: Subject<void> = new Subject<void>();

  constructor(
    private _route: ActivatedRoute,
    private _mobileService: MobilePhonesService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.getNews();
    this.subcategories = this._mobileService.getSubcategories();
  
  }

  ngOnDestroy(): void {
    this._unsubsrcibe.next();
    this._unsubsrcibe.complete();
  }
  private getNews(): void {
    this.phones = this._mobileService.phones;

  }

  getDetails() {
    this._router.navigate(['/'], { queryParams: { phoneId: this.phones } });
  }
  


}
