import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { PhonesViewComponent } from './phones-view/phones-view.component';
import { PhoneCardComponent } from './phone-card/phone-card.component';
import { RouterModule } from '@angular/router';
import { ManufacturComponent } from './manufactur/manufactur.component';




@NgModule({
  declarations: [
    PhonesViewComponent,
    PhoneCardComponent,
    ManufacturComponent,
 
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    RouterModule
  ],
  exports: [
    PhonesViewComponent,
    PhoneCardComponent,
  
  ]
})
export class SharedModule { }
