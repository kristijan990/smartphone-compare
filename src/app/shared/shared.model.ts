export class Phones {
    constructor(

      public phoneId?: string,
      public phonesScreen?: string,
      public phoneResolution?: string,
      public phoneModel?: string,
      public phoneName?: string,
      public phoneImage?: string,
      public phoneRam?: string,
      public phoneChip?: string,
      public phonePrice?: string,
      public phoneCamera?: string,
      public phoneVideo?: string,
      public phoneBattery?: string,
      public phoneStorage?: string,
      public subcategory?: string,
    ){}
  }

  export class Subcategory {
    constructor(
      public name?: string,
      public title?: string){}
  }
