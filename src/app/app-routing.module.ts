import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhonesDetailsComponent } from './mobile-phones/phones-details/phones-details.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [

  {
    path:'',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path:'phones',
    loadChildren: () => import('./mobile-phones/mobilephones.module').then((m) => m.MobilePhonesModule),
  },
  {
    path:'compare',
    loadChildren: () => import('./compare/compare.module').then((m) => m.CompareModule),
  },
  {
    path:'contact',
    loadChildren: () => import('./contact/contact.module').then((m) => m.ContactModule),
  },
  // {
  //   path:'service',
  //   loadChildren: () => import('./service/service.module').then((m) => m.ServiceModule),
  // },
  {
    path: 'phone/:phoneId',
    component: PhonesDetailsComponent
  },
  {
    path:'**',
    redirectTo:''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
