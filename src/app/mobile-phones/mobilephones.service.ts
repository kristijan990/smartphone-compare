import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Phones, Subcategory } from '../shared/shared.model';

@Injectable({
  providedIn: 'root'
})
export class MobilePhonesService {

  showPhoneNav: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(true);

  public phones: Phones[] = [
    {
      phoneId: 'iphone1',
      phoneModel:'iPhone 13 Pro',
      phoneName: 'Apple',
      phonesScreen: '6.1"',
      phoneResolution: '1170x2532',
      phoneImage: 'https://ledikom.mk/images/iphone13pro-509289.jpg',
      phoneRam: `6GB`,
      phoneChip: 'Apple A15 Bionic (5 nm)',
      phonePrice: '63.290',
      phoneCamera: '12 MP, f/1.5, 26mm (wide), 1.9µm, dual pixel PDAF, sensor-shift OIS 12 MP, f/2.8, 77mm (telephoto), PDAF, OIS, 3x optical zoom 12 MP, f/1.8, 13mm, 120˚ (ultrawide), PDAF TOF 3D LiDAR scanner (depth)',
      phoneVideo: '4K@24/30/60fps, 1080p@30/60/120/240fps, 10‑bit HDR, Dolby Vision HDR (up to 60fps), ProRes, Cinematic mode, stereo sound rec.',
      phoneBattery: 'Li-Ion',
      phoneStorage: '128GB, 256GB, 512GB, 1TB',
      subcategory: 'Apple'
    },
    {
      phoneId: 'iphone2',
      phoneModel:'iPhone 13',
      phoneName: 'Apple',
      phonesScreen: '6.1"',
      phoneResolution: '1170x2532',
      phoneImage: '../../assets/images/iphone13-203995.jpg',
      phoneRam: `4GB`,
      phoneChip: 'Apple A15 Bionic (5 nm)',
      phonePrice: '45.490',
      phoneCamera: '12 MP, f/1.6, 26mm (wide), 1.7µm, dual pixel PDAF, sensor-shift OIS 12 MP, f/2.4, 120˚, 13mm (ultrawide)',
      phoneVideo: '4K@24/30/60fps, 1080p@30/60/120/240fps, HDR, Dolby Vision HDR (up to XXfps), stereo sound rec.',
      phoneBattery: 'Li-Ion, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'Apple'
    },
    {
      phoneId: 'iphone3',
      phoneModel:'iPhone 13 Pro Max',
      phoneName: 'Apple',
      phonesScreen: '6.7"',
      phoneResolution: '1284x2778',
      phoneImage: 'https://ledikom.mk/images/iphone13pro-671275.jpg',
      phoneRam: `6GB`,
      phoneChip: 'Apple A15 Bionic (5 nm)',
      phonePrice: '69.490',
      phoneCamera: '12 MP, f/1.5, 26mm (wide), 1.9µm, dual pixel PDAF, sensor-shift OIS 12 MP, f/2.8, 77mm (telephoto), PDAF, OIS, 3x optical zoom 12 MP, f/1.8, 13mm, 120˚ (ultrawide), PDAF TOF 3D LiDAR scanner (depth)',
      phoneVideo: '4K@24/30/60fps, 1080p@30/60/120/240fps, 10‑bit HDR, Dolby Vision HDR (up to 60fps), ProRes, Cinematic mode, stereo sound rec.',
      phoneBattery: 'Li-Ion 4352 mAh, non-removable (16.75 Wh)',
      phoneStorage: '128GB, 256GB, 512GB, 1TB',
      subcategory: 'Apple'
    },
    {
      phoneId: 'iphone4',
      phoneModel:'iPhone 11',
      phoneName: 'Apple',
      phonesScreen: '6.1"',
      phoneResolution: '828x1792',
      phoneImage: 'https://ledikom.mk/images/iphone11-22752.jpg',
      phoneRam: `4GB`,
      phoneChip: 'Apple A13 Bionic',
      phonePrice: '31.990',
      phoneCamera: '12 MP, f/1.8, 26mm (wide), 1/2.55", 1.4µm, PDAF, OIS 12 MP, f/2.4, 13mm (ultrawide)',
      phoneVideo: '2160p@24/30/60fps, 1080p@30/60/120/240fps, HDR, stereo sound rec.',
      phoneBattery: 'Li-Ion 3046 mAh battery',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'Apple'
    },
    {
      phoneId: 'iphone5',
      phoneModel:'iPhone 12',
      phoneName: 'Apple',
      phonesScreen: '6.1"',
      phoneResolution: '1170x2532',
      phoneImage: 'https://ledikom.mk/images/iphone12-962731.jpg',
      phoneRam: `4GB`,
      phoneChip: 'Apple A14 Bionic (5 nm)',
      phonePrice: '39.290',
      phoneCamera: '12 MP, f/1.6, 26mm (wide), 1.4µm, dual pixel PDAF, OIS 12 MP, f/2.4, 120˚, 13mm (ultrawide), 1/3.6"',
      phoneVideo: '4K@24/30/60fps, 1080p@30/60/120/240fps, HDR, Dolby Vision HDR (up to 30fps), stereo sound rec.',
      phoneBattery: 'Li-Ion 2815 mAh, non-removable (10.9 Wh)',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'Apple'
    },
    {
      phoneId: 'samsung1',
      phoneName: 'Samsung',
      phoneModel:'Galaxy S22 Ultra 5G',
      phonesScreen: '6.8"',
      phoneResolution: '1440x3088',
      phoneImage: 'https://ledikom.mk/images/galaxys22ultra5g-571180.jpg',
      phoneRam: `12GB`,
      phoneChip: 'Exynos 2200',
      phonePrice: '79.890',
      phoneCamera: '108 MP, f/1.8, 23mm (wide), 1/1.33", 0.8µm, PDAF, Laser AF, OIS 10 MP, f/4.9, 230mm (periscope telephoto), 1/3.52", 1.12µm, dual pixel PDAF, OIS, 10x optical zoom 10 MP, f/2.4, 70mm (telephoto), 1/3.52", 1.12µm, dual pixel PDAF, OIS, 3x optical zoom 12 MP, f/2.2, 13mm, 120˚ (ultrawide), 1/2.55", 1.4µm, dual pixel PDAF, Super Steady video',
      phoneVideo: '8K@24fps, 4K@30/60fps, 1080p@30/60/240fps, 720p@960fps, HDR10+, stereo sound rec., gyro-EIS',
      phoneBattery: 'Li-Ion 5000 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB, 1TB',
      subcategory: 'samsung'
    },
    {
      phoneId: 'samsung2',
      phoneName: 'Samsung',
      phoneModel:'Galaxy S21 Ultra 5G 12',
      phonesScreen: '6.8"',
      phoneResolution: '1440x3200',
      phoneImage: 'https://ledikom.mk/images/galaxys21ultra5g-636035.jpg',
      phoneRam: `12GB`,
      phoneChip: 'Qualcomm SM8350 Snapdragon 888 (5 nm) - USA/China',
      phonePrice: '54.890',
      phoneCamera: '108 MP, f/1.8, 24mm (wide), 1/1.33", 0.8µm, PDAF, Laser AF, OIS 10 MP, f/4.9, 240mm (periscope telephoto), 1/3.24", 1.22µm, dual pixel PDAF, OIS, 10x optical zoom 10 MP, f/2.4, 70mm (telephoto), 1/3.24", 1.22µm, dual pixel PDAF, OIS, 3x optical zoom 12 MP, f/2.2, 13mm (ultrawide), 1/2.55", 1.4µm, dual pixel PDAF, Super Steady video',
      phoneVideo: '8K@24fps, 4K@30/60fps, 1080p@30/60/240fps, 720p@960fps, HDR10+, stereo sound rec., gyro-EIS',
      phoneBattery: 'Li-Ion 5000 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'samsung'
    },
    {
      phoneId: 'samsung3',
      phoneName: 'Samsung',
      phoneModel:'Galaxy Z Flip3 5G',
      phonesScreen: '6.7"',
      phoneResolution: '1080x2640',
      phoneImage: 'https://ledikom.mk/images/galaxyzflip35g-711342.jpg',
      phoneRam: `8GB`,
      phoneChip: 'Qualcomm SM8350 Snapdragon 888 5G (5 nm)',
      phonePrice: '44.290',
      phoneCamera: '12 MP, f/1.8, 27mm (wide), 1/2.55", 1.4µm, Dual Pixel PDAF, OIS 12 MP, f/2.2, 123˚ (ultrawide), 1.12µm',
      phoneVideo: '4K@30/60fps, 1080p@60/240fps, 720p@960fps, HDR10+',
      phoneBattery: 'Li-Po 3300 mAh, non-removable',
      phoneStorage: '128GB, 256GB',
      subcategory: 'samsung'
    },
    {
      phoneId: 'samsung4',
      phoneName: 'Samsung',
      phoneModel:'Galaxy Z Fold3 5G',
      phonesScreen: '7.6"',
      phoneResolution: '1768x2208',
      phoneImage: 'https://ledikom.mk/images/galaxyzfold35g-874417.jpg',
      phoneRam: `12MB`,
      phoneChip: 'Qualcomm SM8350 Snapdragon 888 5G (5 nm)',
      phonePrice: '78.090',
      phoneCamera: '12 MP, f/1.8, 26mm (wide), 1/1.76", 1.8µm, Dual Pixel PDAF, OIS 12 MP, f/2.4, 52mm (telephoto), 1/3.6", 1.0µm, PDAF, OIS, 2x optical zoom 12 MP, f/2.2, 123˚, 12mm (ultrawide), 1.12µm',
      phoneVideo: '4K@60fps, 1080p@60/240fps (gyro-EIS), 720p@960fps (gyro-EIS), HDR10+',
      phoneBattery: 'Li-Ion',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'samsung'
    },
    {
      phoneId: 'samsung5',
      phoneName: 'Samsung',
      phoneModel:'Galaxy S22 5G',
      phonesScreen: '6.1"',
      phoneResolution: '1080x2340',
      phoneImage: 'https://ledikom.mk/images/galaxys225g-891851.jpg',
      phoneRam: '8GB',
      phoneChip: 'Exynos 2200 (4 nm) - Europe Qualcomm SM8450 Snapdragon 8 Gen 1 (4 nm) - ROW',
      phonePrice: '54.790',
      phoneCamera: '50 MP, f/1.8, 23mm (wide), 1/1.56", 1.0µm, Dual Pixel PDAF, OIS 10 MP, f/2.4, 70mm (telephoto), 1/3.94", 1.0µm, PDAF, OIS, 3x optical zoom 12 MP, f/2.2, 13mm, 120˚ (ultrawide), 1/2.55" 1.4µm, Super Steady video',
      phoneVideo: '8K@24fps, 4K@30/60fps, 1080p@30/60/240fps, 720p@960fps, HDR10+, stereo sound rec., gyro-EIS',
      phoneBattery: 'Li-Ion 3700 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'samsung'
    },
    {
      phoneId: 'xiaomi1',
      phoneName: 'Xiaomi',
      phoneModel:'Mi 11 Ultra',
      phonesScreen: '6.81"',
      phoneResolution: '1440x3200',
      phoneImage: 'https://ledikom.mk/images/mi11ultra12gb256gbceramicblack-49170.jpg',
      phoneRam: `12GB`,
      phoneChip: 'Qualcomm SM8350 Snapdragon 888 5G (5 nm)',
      phonePrice: '53.990',
      phoneCamera: '50 MP, f/2.0, 24mm (wide), 1/1.12", 1.4µm, Dual Pixel PDAF, Laser AF, OIS 48 MP, f/4.1, 120mm (periscope telephoto), 1/2.0", 0.8µm, PDAF, OIS, 5x optical zoom 48 MP, f/2.2, 12mm, 128˚ (ultrawide), 1/2.0", 0.8µm, PDAF',
      phoneVideo: '8K@24fps, 4K@30/60fps, 1080p@30/60/120/240/960/1920fps, gyro-EIS, HDR10+ rec.',
      phoneBattery: 'Li-Po 5000 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'xiaomi'
    },
    {
      phoneId: 'xiaomi2',
      phoneName: 'Xiaomi',
      phoneModel:'11 T',
      phonesScreen: '6.67"',
      phoneResolution: '1080x2400',
      phoneImage: 'https://ledikom.mk/images/xiaomi11t2-1027546.jpg',
      phoneRam: `8GB`,
      phoneChip: 'MediaTek MT6893 Dimensity 1200 5G (6 nm)',
      phonePrice: '26.990',
      phoneCamera: '108 MP, f/1.8, 26mm (wide), 1/1.52", 0.7µm, PDAF 8 MP, f/2.2, 120˚ (ultrawide), 1/4", 1.12µm 5 MP, f/2.4, 50mm (telephoto macro), 1/5.0", 1.12µm, AF',
      phoneVideo: '4K@30fps, 1080p@30/60/120, gyro-EIS',
      phoneBattery: 'Li-Po 5000 mAh, non-removable',
      phoneStorage: '128GB, 256GB',
      subcategory: 'xiaomi'
    },
    {
      phoneId: 'xiaomi3',
      phoneName: 'Xiaomi',
      phoneModel:'Black Shark 3',
      phonesScreen: '6.67"',
      phoneResolution: '1080x2400',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/5ee/4cc/b5a/thumb_2170_550_800_0_0_crop.png',
      phoneRam: `12GB`,
      phoneChip: 'Qualcomm SM8250 Snapdragon 865 (7 nm+)',
      phonePrice: '26,390',
      phoneCamera: '64 MP, f/1.8, 26mm (wide), 1/1.72", 0.8µm, PDAF 13 MP, f/2.3, (ultrawide) 5 MP, f/2.2, (depth)',
      phoneVideo: '4K@30/60fps, 1080p@30/60/240fps, 720p@1920fps',
      phoneBattery: 'Li-Po Non-removable 4720 mAh battery',
      phoneStorage: '128GB, 256GB, 512GB,',
      subcategory: 'xiaomi'
    },
    {
      phoneId: 'xiaomi4',
      phoneName: 'Xiaomi',
      phoneModel:'Mi 10T PRO 5G',
      phonesScreen: '6.67"',
      phoneResolution: '1080x2400',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/606/c3d/318/thumb_2668_550_800_0_0_crop.png',
      phoneRam: `8GB`,
      phoneChip: 'Qualcomm SM8250 Snapdragon 865 5G (7 nm+)',
      phonePrice: '30,690',
      phoneCamera: '108 MP, f/1.7, 26mm (wide), 1/1.33", 0.8µm, PDAF, OIS 13 MP, f/2.4, 123˚ (ultrawide), 1.12µm 5 MP, f/2.4, (macro), 1/5.0", 1.12µm, AF',
      phoneVideo: '8K@30fps, 4K@30/60fps, 1080p@30/60/120fps; gyro-EIS',
      phoneBattery: 'Li-Po 5000 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'xiaomi'
    },
    {
      phoneId: 'xiaomi4',
      phoneName: 'Xiaomi',
      phoneModel:'Black Shark 4',
      phonesScreen: '6.67"',
      phoneResolution: '1080x2400',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/614/9ee/41c/thumb_3352_550_800_0_0_crop.png',
      phoneRam: `8G , 12GB`,
      phoneChip: 'Qualcomm SM8250-AC Snapdragon 870 5G (7 nm)',
      phonePrice: '28,900',
      phoneCamera: '48 MP, f/1.8, (wide), 1/2.0", 0.8µm, PDAF 8 MP, f/2.2, 120˚ (ultrawide), 1/4.0", 1.12µm 5 MP, f/2.4, (macro), AF',
      phoneVideo: '4K@30/60fps, 1080p@30/60/240fps, 1080p@960fps',
      phoneBattery: 'Li-Po 4500 mAh, non-removable',
      phoneStorage: '128GB, 256GB',
      subcategory: 'xiaomi'
    },
    {
      phoneId: 'huawei1',
      phoneName: 'Huawei',
      phoneModel:'P50 Pocket 5G',
      phonesScreen: '6.9"',
      phoneResolution: '1188x2790',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/621/a31/b32/thumb_3662_550_800_0_0_crop.png',
      phoneRam: `12GB`,
      phoneChip: 'Qualcomm SM8350 Snapdragon 888 4G (5 nm)',
      phonePrice: '76,900',
      phoneCamera: '40 MP, f/1.8, (wide), PDAF, Laser AF 13 MP, f/2.2, 120˚ (ultrawide), AF 32 MP, f/1.8, (wide), 1/3.14", 0.7µm, AF',
      phoneVideo: '4K@30/60fps, 1080p@30/120/240fps, gyro-EIS',
      phoneBattery: 'Li-Po 4000 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'huawei'
    },
    {
      phoneId: 'huawei2',
      phoneName: 'Huawei',
      phoneModel:'P50 Pro 5G',
      phonesScreen: '6.6"',
      phoneResolution: '1228x2700',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/621/a30/2d8/thumb_3658_550_800_0_0_crop.png',
      phoneRam: `12GB`,
      phoneChip: 'Kirin 9000 (5 nm) Qualcomm SM8350 Snapdragon 888 4G (5 nm)',
      phonePrice: '58,400',
      phoneCamera: '50 MP, f/1.8, 23mm (wide), PDAF, Laser AF, OIS 64 MP, f/3.5, 90mm (periscope telephoto), PDAF, OIS, 3.5x optical zoom, 7x lossless zoom 13 MP, f/2.2, 13mm (ultrawide), AF 40 MP, f/1.6, 23mm (B/W), AF',
      phoneVideo: '4K@30/60fps, 1080p@30/60fps, 1080p@960fps; gyro-EIS',
      phoneBattery: 'Li-Po 4360 mAh, non-removable',
      phoneStorage: '128GB, 256GB, 512GB',
      subcategory: 'huawei'
    },
    {
      phoneId: 'huawei3',
      phoneName: 'Huawei',
      phoneModel:'Honor 8X Max',
      phonesScreen: '7.12"',
      phoneResolution: '1080x2244',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/5cd/051/f4c/thumb_1809_550_800_0_0_crop.jpg',
      phoneRam: `6GB`,
      phoneChip: 'Qualcomm SDM636 Snapdragon 636 (14 nm)',
      phonePrice: '12,300',
      phoneCamera: '16 MP, f/2.0, PDAF 2 MP, f/2.4, depth sensor',
      phoneVideo: '1080p@30fps',
      phoneBattery: 'Li-Po 5000 mAh Non-removable battery',
      phoneStorage: '64GB, 128GB',
      subcategory: 'huawei'
    },
    {
      phoneId: 'huawei4',
      phoneName: 'Huawei',
      phoneModel:'P40 lite E',
      phonesScreen: '6.39"',
      phoneResolution: '720x1560',
      phoneImage: 'https://mobelix.com.mk/storage/app/uploads/public/5f2/bc0/63d/thumb_2194_550_800_0_0_crop.jpg',
      phoneRam: `4GB`,
      phoneChip: 'Kirin 710F (12 nm)',
      phonePrice: '9,200',
      phoneCamera: '48 MP, f/1.8, 27mm (wide), 1/2.0", 0.8µm, PDAF 8 MP, f/2.4, (ultrawide), 1/4.0", 1.12µm 2 MP, f/2.4, (depth)',
      phoneVideo: '1080p@30fps',
      phoneBattery: 'Li-Po 4000 mAh Non-removable battery',
      phoneStorage: '64GB',
      subcategory: 'huawei'
    }

  ];

  private _subcategories: Subcategory[] = [
    {
      name: 'Apple',
      title: 'Apple'
    },
    {
      name: 'samsung',
      title: 'Samsung'
    },
    {
      name: 'xiaomi',
      title: 'Xiaomi'
    },
    {
      name: 'huawei',
      title: 'Huawei'
    }
  ]


  constructor() { }


  public getPhonesById(phoneId: string): Phones[]{
    return this.phones.filter(n => n.phoneId === phoneId);
  }

  public getNewsBySubcategory(subcategory: string): Phones[]{
    return this.phones.filter(n => n.subcategory === subcategory);
  }

  public getSubcategories(): Subcategory[] {
    return this._subcategories;
  }
}
