import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MobilephonesComponent } from "./mobilephones.component";
import { PhonesViewComponent } from "../shared/phones-view/phones-view.component";
import { ManufacturComponent } from "../shared/manufactur/manufactur.component";
import { PhonesDetailsComponent } from "./phones-details/phones-details.component";

const phonesRoutes: Routes = [
  {
    path: '',
    component: MobilephonesComponent,
    children: [
      {
        path: '',
        component: PhonesViewComponent,
      },
      {
        path: ':phones-subcategory',
        component: ManufacturComponent
      },


    ]
  },
  {
    path:'**',
    redirectTo:''
  }
];

@NgModule({
  imports: [RouterModule.forChild(phonesRoutes)],
  exports: [RouterModule],
})
export class MobilePhonesRoutingModule {}
