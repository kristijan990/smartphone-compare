import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Phones, Subcategory } from 'src/app/shared/shared.model';
import { MobilePhonesService } from '../mobilephones.service';

@Component({
  selector: 'app-phones-details',
  templateUrl: './phones-details.component.html',
  styleUrls: ['./phones-details.component.scss']
})
export class PhonesDetailsComponent implements OnInit {

  public news: Phones[];
  public subcategories: Subcategory[];
  private _unsubsrcibe: Subject<void> = new Subject<void>();

  public phone: Phones;

  phoneId: string = null;

  constructor(
    private _mobileService: MobilePhonesService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.subcategories = this._mobileService.getSubcategories();
    console.log('subcategories -> ', this.subcategories)
    this.getNews();

    this._route.params.subscribe(
      (params: Params) => {
        this.phoneId = params['phoneId']
        if(this.phoneId != null){
          this.getPhonesDetails();
        }
        console.log("details" , this.phoneId)
      }
    )

    console.log(this._route.snapshot.queryParams)
  }

  getPhonesDetails() {

    let phoneList = this._mobileService.phones;

    this.phone = phoneList.find((singlePhones: Phones) => singlePhones.phoneId == this.phoneId)!
  }

  private getNews(): void {
    this._route.params.pipe(takeUntil(this._unsubsrcibe)).subscribe(
      (params: Params) => {
        this.news = this._mobileService.getNewsBySubcategory('');
  
        if (params['news-subcategory']) {

          let subcategory = params['news-subcategory'];
          console.log('NEWS PARAMS: ', subcategory);

          if(subcategory) {

            this.news = this._mobileService.getNewsBySubcategory(subcategory);

            console.log('news 222 -> ', this.news)
            if(this.news.length == 0) {
              this._router.navigate(['phones'])
            }

          } else {
            this._router.navigate(['phones'])
          }


        }

      }
    );
  }
}
