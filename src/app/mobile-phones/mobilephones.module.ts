import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobilephonesComponent } from './mobilephones.component';
import { MobilePhonesRoutingModule } from './mobilephones-routing.module';
import { PhonesDetailsComponent } from './phones-details/phones-details.component';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    MobilephonesComponent,
    PhonesDetailsComponent
  ],
  imports: [
    CommonModule,
    MobilePhonesRoutingModule,
    MatCardModule
  ]
})
export class MobilePhonesModule { }
