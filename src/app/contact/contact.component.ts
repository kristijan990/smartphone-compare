import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { MobilePhonesService } from '../mobile-phones/mobilephones.service';
import { Phones, Subcategory } from '../shared/shared.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public phones: Phones[];
  public subcategories: Subcategory[];
  private _unsubsrcibe: Subject<void> = new Subject<void>();

  constructor(
    private _mobileService: MobilePhonesService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.subcategories = this._mobileService.getSubcategories();
    console.log('subcategories -> ', this.subcategories)
    this.getNews();
  }

  private getNews(): void {
    this._route.params.pipe(takeUntil(this._unsubsrcibe)).subscribe(
      (params: Params) => {
        this.phones = this._mobileService.getNewsBySubcategory('');
        console.log('NEWS: ', this.phones);
        if (params['phones-subcategory']) {

          let subcategory = params['phones-subcategory'];
          console.log('phones PARAMS: ', subcategory);

          if(subcategory) {
            //Get the news from the News service
            this.phones = this._mobileService.getNewsBySubcategory(subcategory);

            console.log('phones 222 -> ', this.phones)
            if(this.phones.length == 0) {
              this._router.navigate(['phones'])
            }

          } else {
            this._router.navigate(['phones'])
          }

        }

      }
    );
  }

 }


