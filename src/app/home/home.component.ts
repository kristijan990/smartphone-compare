import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig, NguCarouselStore } from '@ngu/carousel';
import { MobilePhonesService } from '../mobile-phones/mobilephones.service';
import { Phones, Subcategory } from '../shared/shared.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public subcategories: Subcategory[] = [];
  

  constructor(
        private _phonesService: MobilePhonesService
        ) { }

  ngOnInit(): void {
    this.subcategories = this._phonesService.getSubcategories();
   
  }


}
