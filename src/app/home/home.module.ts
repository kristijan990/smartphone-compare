import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import { SharedModule } from '../shared/shared.module';
import { NguCarouselModule } from '@ngu/carousel';


@NgModule({
  declarations: [
    HomeComponent,
  
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatSidenavModule,
    SharedModule,
    NguCarouselModule
  ]
})
export class HomeModule { }
