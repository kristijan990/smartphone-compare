import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PhonesViewComponent } from "../shared/phones-view/phones-view.component";
import { HomeComponent } from "./home.component";

const homeRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
