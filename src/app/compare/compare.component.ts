import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map, Observable, startWith, Subject, takeUntil } from 'rxjs';
import { MobilePhonesService } from '../mobile-phones/mobilephones.service';
import { Phones, Subcategory } from '../shared/shared.model';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {
  public phones: Phones[];
  public subcategories: Subcategory[];
  private _unsubsrcibe: Subject<void> = new Subject<void>();

  phoneForm1 : FormControl = new FormControl();
  phoneForm2 : FormControl = new FormControl();

  options1: Phones[] = new Array<Phones>();
  options2: Phones[] = new Array<Phones>();

  filteredOptions1: Observable<Phones[]> = new Observable<Phones[]>();
  filteredOptions2: Observable<Phones[]> = new Observable<Phones[]>();


  constructor(
    private _mobileService: MobilePhonesService,
    private _route: ActivatedRoute,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.options1 = this.options1.concat(this._mobileService.phones);
    this.options2 = this.options2.concat(this._mobileService.phones);


    this.subcategories = this._mobileService.getSubcategories();
    console.log('subcategories -> ', this.subcategories)
    this.getNews();

    this.filteredOptions2 = this.phoneForm2.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : (value.phoneName || value.phoneModel)),
      map(value => (value ? this._filter2(value) : this.options1.slice())),

    );


    this.filteredOptions1 = this.phoneForm1.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : (value.phoneName || value.phoneModel)),
      map(value => (value ? this._filter1(value) : this.options2.slice())),
    );

  }

  displayPhone1(phone: Phones): string {
    console.log('phone1' , phone);
    return phone && phone.phoneName? phone.phoneName + ' ' + phone.phoneModel : '';

  }

  displayPhone2(phone2: Phones): string {
    console.log('phone2' , phone2);
    return phone2 && phone2.phoneName? phone2.phoneName + ' ' + phone2.phoneModel : '';
  }

  private _filter1(value: string): Phones[] {
    const filterValue = value.toLowerCase();
    console.log("model1 ", value);
    return this.options1.filter(option => (option.phoneName.toLowerCase().includes(filterValue)) || option.phoneModel.toLowerCase().includes(filterValue));

  }

  private _filter2(value: string): Phones[] {

    const filterValue = value.toLowerCase();
    console.log("model2", value);
    return this.options2.filter(option => (option.phoneName.toLowerCase().includes(filterValue)) || option.phoneModel.toLowerCase().includes(filterValue));
  }

  private getNews(): void {
    this._route.params.pipe(takeUntil(this._unsubsrcibe)).subscribe(
      (params: Params) => {
        this.phones = this._mobileService.getNewsBySubcategory('');

        if (params['phones-subcategory']) {

          let subcategory = params['phones-subcategory'];
          console.log('phones PARAMS: ', subcategory);

          if(subcategory) {

            this.phones = this._mobileService.getNewsBySubcategory(subcategory);

            console.log('phones 222 -> ', this.phones)
            if(this.phones.length == 0) {
              this._router.navigate(['phones'])
            }

          } else {
            this._router.navigate(['phones'])
          }

        }

      }
    );
  }

}
