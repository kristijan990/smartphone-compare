import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompareComponent } from "./compare.component";

const comparetRoutes: Routes = [
  {
    path: '',
    component: CompareComponent,
  },
  {
    path: '**',
    redirectTo: 'compare'
  }
];

@NgModule({
  imports: [RouterModule.forChild(comparetRoutes)],
  exports: [RouterModule],
})
export class CompareRoutingModule {}