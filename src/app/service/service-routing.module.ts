import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ServiceComponent } from "./service.component";

const serviceRoutes: Routes = [
  {
    path: '',
    component: ServiceComponent,
  },
  {
    path:'',
    redirectTo:'service'
  }
];

@NgModule({
  imports: [RouterModule.forChild(serviceRoutes)],
  exports: [RouterModule],
})
export class ServiceRoutingModule {}
